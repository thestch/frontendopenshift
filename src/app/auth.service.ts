import { Injectable } from '@angular/core';
import Amplify from '@aws-amplify/core';
import {Auth} from '@aws-amplify/auth';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  async signUp(username: string, password: string, email: string, phone_number: string) {
    try {
        const { user } = await Auth.signUp({
            username,
            password,
            attributes: {
                email,          // optional
                phone_number,   // optional - E.164 number convention
                // other custom attributes 
            }
        });
        console.log(user);
    } catch (error) {
        console.log('error signing up:', error);
    }
}



async signIn(username: string, password: string) {
    try {
        const user = await Auth.signIn(username, password);
        return Promise.resolve();
    } catch (error) {
        console.log('error signing in', error);
        return Promise.reject(error);
    }
}


async resendConfirmationCode(username: string) {
    try {
        await Auth.resendSignUp(username);
        console.log('code resent successfully');
    } catch (err) {
        console.log('error resending code: ', err);
    }
}

async signOut() {
    try {
        await Auth.signOut();
    } catch (error) {
        console.log('error signing out: ', error);
    }
}
}
