interface Callback {
    OnSuccess(success: any): void
    OnError(error: any): void
}