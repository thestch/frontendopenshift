import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Auth } from '@aws-amplify/auth';

@Component({
  selector: 'app-mfa',
  templateUrl: './mfa.component.html',
  styleUrls: ['./mfa.component.css']
})
export class MfaComponent implements OnInit {
  formdata: any;
  private sub: any;
  private username: string;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    this.sub = this.route.params.subscribe(params => {
      this.username = params['username']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });

    this.formdata = new FormGroup({
      code: new FormControl(""),
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public async confirmSignUp() {
    console.log(this.formdata.value)

    try {
        await Auth.confirmSignUp(this.username, this.formdata.value.code);
        this.router.navigate(['users']);

    } catch (error) {
        console.log('error confirming sign up', error);
    }
}


}
